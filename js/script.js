function calcFactorial(number) {
    let factorial = 1;
    for (number; number > 1; number--) {
        factorial *= number;
    }
    return factorial;
}

let number = +prompt('Enter number for factorial calculation');

while (isNaN(number) === true) {
    number = +prompt('Enter number!!! for factorial calculation');
}

alert(calcFactorial(number));